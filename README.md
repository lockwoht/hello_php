# Learning PHP for ILDA
## Starting small

[The PHP Manual](https://www.php.net/manual/en/) has all sorts of things that I'll need to check, like the [Language Reference](https://www.php.net/manual/en/langref.php) and [A Simple Tutorial](https://www.php.net/manual/en/tutorial.php).

Through a GitHub search I found [odan/learn-php](https://github.com/odan/learn-php?tab=readme-ov-file#must-read).

Some notes on *[PHP: The Right Way](https://phptherightway.com/#use_the_current_stable_version)*, which I found there, and was helpful.

