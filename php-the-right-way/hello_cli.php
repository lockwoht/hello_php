<?php
if ($argc !== 2) {
    echo "Usage: php hello.php <name>" . PHP_EOL;
    exit(1);
} else {
    $name = $argv[1];
}

if ($name == "Odysseus") {
    echo "Man of many ways, loved by Zeus..." . PHP_EOL;
} elseif ($name == "Hunter") {
    echo "Walk tall, kick ass, learn to speak Arabic, love music and never forget you come from a long line of truth seekers, lovers and warriors. -Hunter S. Thompson" . PHP_EOL;
} else {
    $name = $argv[1];
    echo "Hello, $name" . PHP_EOL;
}

?>
