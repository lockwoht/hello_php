Q: How do I start the homebrew php service...

# *Getting Started*

[Chapter 2](https://phptherightway.com/#use_the_current_stable_version)

## Starting a server

```shell
php -S localhost:8000

```

See also the [built-in CLI server: docs](https://www.php.net/features.commandline.webserver)

## **Standard PHP Package Skeleton**

(On Github @ [php-pds/skeleton](https://github.com/php-pds/skeleton))

Obviously not all PHP projects have all of these, but the standard is:

- root-level directories (explanations if needed)
    - `bin/`
    - `config/` 
    - `docs/`
    - `public/` (web server files)
    - `resources/`
    - `src/` (PHP source code)
    - `tests/`
- root-level files (explanations if needed)  
    - `CHANGELOG(.*)`
    - `CONTRIBUTING(.*)`
    - `LICENSE(.*)`
    - `README(.*)`  

# Code Style Guide

[Chapter 3](https://phptherightway.com/#code_style_guide)

**tl;dr:** Since PHP is old & diverse, there are a lot of styles & it's important to be consistent & interoperative.

## Which Standard?

I guess I'm just going to worry about [Symfony Coding Standards](https://symfony.com/doc/current/contributing/code/standards.html) until I get reason to do something different:

> These Symfony coding standards are based on the PSR-1, PSR-2, PSR-4 and PSR-12 standards, so you may already know most of them.

### TODO: Linters & LSPs?

Symfony recommends [installing the PHP Coding Standards Fixer](https://cs.symfony.com/), which then just gets invoked in the project folder

```shell
mkdir --parents tools/php-cs-fixer
composer require --working-dir=tools/php-cs-fixer friendsofphp/php-cs-fixer

...

cd your-project/
php php-cs-fixer.phar fix -v
  
```

My editor of choice, [Helix, provides configuration details](https://github.com/helix-editor/helix/wiki/How-to-install-the-default-language-servers#php) on setting up [Phpactor](https://phpactor.readthedocs.io/en/master/index.html); here's the guide for [integrating with Symfony](https://phpactor.readthedocs.io/en/master/integrations/symfony.html).
