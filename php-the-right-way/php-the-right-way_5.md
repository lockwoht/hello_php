# Dependency Management

[Chapter 5](https://phptherightway.com/#dependency_management)

## Composer

I got shuffled into `composer` through the Laravel materials, so let's go.

### Managing dependencies with `composer.json`

Edit by hand or use the tool:

```shell using_composer
composer init # guided `composer.json` file creation
composer require twig/twig:^2.0 # add specific dependencies
composer install # download & install dependencies to `vendor/` & sets lockfile
composer update # when you're ready

```

In your file, just `require` it using familiar syntax:

```php require_dependencies
<?php
require 'vendor/autoload.php';

```

...and see [the Local PHP Security Checker](https://github.com/fabpot/local-php-security-checker).
