# Language Highlights

[Chapter 4](https://phptherightway.com/#language_highlights)

## "PHP is a flexible, dynamic language..."

### Object-oriented Programming

"...classes, abstract classes, interfaces, inheritance, constructors, cloning, exceptions..."

[see also the php.net docs](https://www.php.net/language.oop5)

### Functional Programming

"first-class functions...Higher-order Functions...recursion...anonymous functions...the ability to bind closures to an object's scope..."

[Their discussion continues here...](https://phptherightway.com/pages/Functional-Programming.html)

A simple example from that page using the built-in `array_filter()`:

```php array_filter()
<?php
$input = array(1, 2, 3, 4, 5, 6);

// Creates a new anonymous function and assigns it to a variable
$filter_even = function($item) {
    return ($item % 2) == 0;
};

// Built-in array_filter accepts both the data and the function
$output = array_filter($input, $filter_even);

// The function doesn't need to be assigned to a variable. This is valid too:
$output = array_filter($input, function($item) {
    return ($item % 2) == 0;
});

print_r($output);

```

### Meta Programming 

(skipped for now)

## Namespaces

[from php.net docs](https://www.php.net/language.namespaces)

Must be first statement other than `declare` which handles character encoding. 

```php single_namespace
<?php
declare(encoding='UTF-8');
namespace MyProject;

const CONNECT_OK = 1;
class Connection { /* ... */ }
function connect() { /* ... */ }

?>

```

Namespaces can be defined across the filesystem. Multiple namespaces can be defined in a single file, but don't. Sub-namespaces can be defined using `\`.

```php multiple_and_subnamespaces
<?php
namespace MyProject {

const CONNECT_OK = 1;
class Connection { /* ... */ }
function connect() { /* ... */  }
}

namespace AnotherProject\MySublevel {

const CONNECT_OK = 1;
class Connection { /* ... */ }
function connect() { /* ... */  }
}
?>

```

There's another syntax to do multiple namespaces, but it's not compatible with the mixing of namespaced & un-namespaced code, which should be done as follows; note how nothing exists outside of the `namespace` brackets.

```php namespace_and_global
<?php
declare(encoding='UTF-8');
namespace MyProject {

const CONNECT_OK = 1;
class Connection { /* ... */ }
function connect() { /* ... */  }
}

namespace { // global code
session_start();
$a = MyProject\connect();
echo MyProject\Connection::start();
}
?>
  
```

## Standard Library

"It is made up primarily of commonly needed datastructure classes (stack, queue, heap, and so on), and iterators..."

[See also the docs](https://www.php.net/book.spl)

## CLI

Start the built-in server:

```shell built-in_server
php -S localhost:$PORTNUMBER # Ctrl+C to turn it off

```

Print PHP configuration the way that `phpinfo()` does:

```shell print_info
php -i
php --info

```

Start an interactive session:

```shell interactive_session
php -a
php --interactive

```

Easy CLI program; since it's run with `php hello.php`, note that the first argument is the filename.

```php hello_name
<?php
if ($argc !== 2) {
    echo "Usage: php hello.php <name>" . PHP_EOL;
    exit(1);
}
$name = $argv[1];
echo "Hello, $name" . PHP_EOL;

```

[see also the docs](https://www.php.net/manual/en/features.commandline.php) for a variety of interesting things like

```shell execute_for_every_input_line
php -R <code>
php --process-code <code>

```

## Debugger

(skipped for now, `printf` wins again)
